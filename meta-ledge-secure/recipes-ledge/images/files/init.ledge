#!/bin/sh

set -e

PATH='/sbin:/bin:/usr/sbin:/usr/bin'
ROOTFS_MOUNT='/rootfs'
INIT='/sbin/init'
# Authenticated state for list of the PCRs
PCR_AUTH='sha256:7,9'
# TPM index of nvram where Rootfs secret is stored
TPM_NVINDEX_ROOTFS="0x8100000a"

atexit () {
	[ "$RNGD_PID" ] && kill "$RNGD_PID"
	[ "$DBUS_PID" ] && kill "$DBUS_PID"
	[ "$ABRMD_PID" ] && kill "$ABRMD_PID"
	modprobe -r tpm_ftpm_tee
	SUPP_ID=$(pgrep tee-supplicant)
	[ "$SUPP_ID" ] && kill "$SUPP_ID"
}

trap atexit EXIT

print_encr_msg() {
	echo ''
	echo '!!!! Warning: erase and encryption is in progress.!!!!'
	echo '!!!! Do not reset/power off your machine until this completes.!!!!'
	echo ''
}

rpmb_cid () {
	for f in /sys/class/mmc_host/mmc*/mmc*\:*/cid; do
		# POSIX shells don't expand globbing patterns that match no file
		[ -e "$f" ] || return
		local devtype=$(echo $f | sed 's/cid/type/')
		[ ! -e "$devtype" ] && continue
		[ $(cat $devtype) != 'MMC' ] && continue
		[ "$cid" ] && { echo "$0": WARNING: multiple eMMC devices found! >&2; return ; }
		local cid=$(cat $f)
	done
	[ "$cid" ] && echo --rpmb-cid "$cid"
}

setup_env() {
	mount -t proc none /proc
	mount -t sysfs none /sys
	mount -t devtmpfs none /dev
	mount -t tmpfs none /run
	mount -t tmpfs none /tmp
	mkdir -p "$ROOTFS_MOUNT"

	modprobe dm_crypt
	modprobe tpm_tis_core
	modprobe tpm_tis

	# fTPM needs to go after the OP-TEE supplicant
	tee-supplicant -d $(rpmb_cid)
	modprobe tpm_ftpm_tee

	# start basic services
	rngd
	RNGD_PID=$(pidof rngd)
	if [ -c '/dev/tpm0' ]; then
		mkdir -p /run/dbus
		dbus-daemon --system
		DBUS_PID=$!
		tpm2-abrmd --allow-root &
		ABRMD_PID=$!
		tpm2_dictionarylockout -c 2> /dev/null
	fi

	echo "Started basic services"
}

update_parsec_config () {
    parsec_config="/$ROOTFS_MOUNT/etc/parsec/config.toml"
    for frag in optee tpm ; do
        fragfile="/$ROOTFS_MOUNT/etc/parsec/config-fragment-$frag.toml"
        [ ! -e "$fragfile" ] && continue
        append=1
        [ "$frag" = 'tpm' -a ! -e '/dev/tpm0' ] && append=""
        [ "$frag" = 'optee' ] && {
            if [ -e '/dev/tee0' ]; then
                echo 'Adding user parsec to group teeclnt'
                "$ROOTFS_MOUNT/usr/sbin/chroot" "$ROOTFS_MOUNT" /usr/sbin/usermod -a -G teeclnt parsec
            else
                append=""
            fi
        }
        [ "$append" ] && {
            echo "Appending $fragfile to $parsec_config"
            echo >>"$parsec_config"
            echo '# Added by init.ledge' >>"$parsec_config"
            cat "$fragfile" >>"$parsec_config"
        }
        echo "Removing $fragfile"
        rm -f "$fragfile"
    done
}

mount_device() {
	local dev="$1"
	local dir="$2"
	local flags='rw,noatime,iversion'

	mount "$dev" "$dir" -o "$flags" || (echo "Mount failed with $?" &&
	    echo "1" > /proc/sys/kernel/sysrq &&
	    echo "s" > /proc/sysrq-trigger &&
	    echo "b" > /proc/sysrq-trigger)
}

# Send trap event 0x00000000 to forbid access to the decryption key past
# the initramfs stage. Update the PCR we seal against so no one can read
# the key.
extend_pcrs() {
	for i in {0..7}; do
		tpm2_pcrextend $i:sha256=0000000000000000000000000000000000000000000000000000000000000000;
	done
}

# tpm2_seal_password - seal password to TPM NVRAM ${TPM_NVINDEX_ROOTFS} index.
# input: $1 - password file
# input: signing_key_public.pem public key
tpm2_seal_password() {
	local passfilename=$1

	cd /tpm2/
	#openssl genrsa -out signing_key_private.pem 2048
	#openssl rsa -in signing_key_private.pem -out signing_key_public.pem -pubout

	tpm2_createprimary -Q --hierarchy=o --key-context=prim.ctx
	tpm2_loadexternal --key-algorithm=rsa --hierarchy=o \
		--public=signing_key_public.pem --key-context=signing_key.ctx \
		--name=signing_key.name > /dev/null
	tpm2_startauthsession --session=session.ctx
	tpm2_policyauthorize --session=session.ctx --policy=authorized.policy --name=signing_key.name > /dev/null
	tpm2_flushcontext session.ctx
	cat ${passfilename} | tpm2_create --hash-algorithm=sha256 \
		 --public=auth_pcr_seal_key.pub --private=auth_pcr_seal_key.priv \
		--sealing-input=- --parent-context=prim.ctx --policy=authorized.policy > /dev/null
	tpm2_load -Q --parent-context=prim.ctx \
		--public=auth_pcr_seal_key.pub --private=auth_pcr_seal_key.priv \
		--name=seal.name --key-context=seal.ctx > /dev/null
	tpm2_evictcontrol -Q -C o -c ${TPM_NVINDEX_ROOTFS} 2> /dev/null || echo "initramfs: TPM NVRAM ${TPM_NVINDEX_ROOTFS} index deleted."
	tpm2_evictcontrol --hierarchy=o --object-context=seal.ctx ${TPM_NVINDEX_ROOTFS} > /dev/null

	cd - > /dev/null
}

# tpm2_unseal_prep - prepare to unseal key from the TPM
# input: (iniramfs)/tpm2/signing_key_public.pem
# input: (initramfs)/<any_name>.pcr.signature
tpm2_unseal_prep() {
	local ret=1
	mkdir -p /tmp/unseal
	cd /tmp/unseal > /dev/null

	cp -r /tpm2/*.pcr.signature .
	cp -r /tpm2/signing_key_public.pem .

	tpm2_startauthsession --policy-session --session=session.ctx
	tpm2_policypcr -Q --session=session.ctx --pcr-list="${PCR_AUTH}" --policy=set2.pcr.policy

	tpm2_loadexternal --key-algorithm=rsa --hierarchy=o \
		--public=signing_key_public.pem \
		--key-context=signing_key.ctx --name=signing_key.name > /dev/null

	for pcr_sig in `ls *.pcr.signature` ; do
		echo "verifying  ${pcr_sig}"
		tpm2_verifysignature --key-context=signing_key.ctx \
			--hash-algorithm=sha256 --message=set2.pcr.policy \
			--signature=${pcr_sig} --ticket=verification.tkt \
			--scheme=rsassa > /dev/null || continue

		tpm2_startauthsession --policy-session --session=session.ctx || continue

		tpm2_policypcr --pcr-list="${PCR_AUTH}" --session=session.ctx \
			--policy=set2.pcr.policy > /dev/null || continue

		tpm2_policyauthorize --session=session.ctx --input=set2.pcr.policy \
			--name=signing_key.name \
			--ticket=verification.tkt > /dev/null || continue

		ret=0
		break;
	done

	if [ $ret -ne 0 ]; then
		tpm2_flushcontext session.ctx
	fi
	cd - > /dev/null

	return $ret
}

# tpm2_unseal_password - print out password
tpm2_unseal_password() {
	cd /tmp/unseal > /dev/null
	tpm2_unseal --auth=session:session.ctx --object-context=${TPM_NVINDEX_ROOTFS}
	tpm2_flushcontext session.ctx > /dev/null
	cd - > /dev/null

}

tpm2_print_pcr_error() {
	echo -e "\n\n\n"
	echo -e "Unable to unseal password\n\n"

	tpm2_startauthsession --session=session.ctx
	tpm2_policypcr -Q --session=session.ctx --pcr-list="${PCR_AUTH}" --policy=set2.pcr.policy
	tpm2_flushcontext session.ctx
	pcr_str=`hexdump -ve '1/1 "%02x"' set2.pcr.policy`
	echo " TPM policy mismatch "
	echo "use: echo -n ${pcr_str} | xxd -r -p  > set2.pcr.policy"
	echo "use: openssl dgst -sha256 -sign signing_key_private.pem -out set2.pcr.signature set2.pcr.policy"
	echo "use: copy set2.pcr.policy to ESP partition default.pcr.pocily"
	echo -e "\n\n"
}

open_enc_blk() {
	tpm2_unseal_prep && \
	    (tpm2_unseal_password |
	    cryptsetup luksOpen --key-file - "$ROOT_DEV" rootfs) || \
	    (dmesg -n1 && tpm2_print_pcr_error)
}

encrypt_fs() {
	print_encr_msg
	mount_device "$ROOT_DEV" '/mnt'
	cd /mnt/
	echo "Backup filesystem to tmpfs ..."
	tar -cf - . | pigz -c >/tmp/rootfs.tar.gz
	cd - > /dev/null
	umount /mnt

	echo "Creating LUKS2 volumes ..."
	#cat /dev/zero > ${ROOT_DEV} 2>/dev/null || true
	# Create a LUKS2 encrypted volume with empty pass
	< /dev/urandom tr -dc _A-Z-a-z-0-9[:punct:] | head -c${1:-32} > /tmp/rand_key

	tpm2_seal_password /tmp/rand_key

	cryptsetup -q --type luks2 --cipher aes-xts-plain --hash sha256 \
		--use-random --uuid="$ROOT_UUID" luksFormat "$ROOT_DEV" \
		--key-file /tmp/rand_key

	echo "Creating encrypted filesystem ..."
        cryptsetup luksOpen --key-file /tmp/rand_key "$ROOT_DEV" rootfs
        shred /tmp/rand_key
        rm /tmp/rand_key

	mkfs.ext4 -q /dev/mapper/rootfs
	mount_device '/dev/mapper/rootfs' "$ROOTFS_MOUNT"
	cd "$ROOTFS_MOUNT"
	unpigz -c /tmp/rootfs.tar.gz | tar xpf -
	cd - > /dev/null
}

try_encrypt() {
	[ ! -c '/dev/tpm0' ] && return 1
	local encr_uuid=`cryptsetup luksUUID $ROOT_DEV`
	if [ -n "$encr_uuid" -a "$encr_uuid" = "$ROOT_UUID" ]; then
		echo "try_encrypt: encryption already done, skip."
		return 1
	fi

	encrypt_fs
}

try_mount() {
	local mount_flags="rw,noatime,iversion"
	local is_luks=`cryptsetup luksUUID $ROOT_DEV`

	echo "Waiting for root device to be ready ..."
	if [ -z "$is_luks" ]; then
		echo "No TPM found.  Mounting unencrypted rootfs"
		mount_device "$ROOT_DEV" "$ROOTFS_MOUNT"
	else
		echo "Mounting encrypted filesystem"
		open_enc_blk
		extend_pcrs
		mount_device '/dev/mapper/rootfs' "$ROOTFS_MOUNT"
	fi
}

find_root_device() {
	local cmdline=$(cat /proc/cmdline)
	local rootfs_string=""
	for arg in $cmdline; do
		optarg=$(expr "x$arg" : 'x[^=]*=\(.*\)' || continue)
		case "$arg" in
		root=*)
			rootfs_string="$optarg"
		;;
		esac
	done

	[ -z "$rootfs_string" ] && \
		echo "exiting on missing the kernel parameter root= ..." && \
		exit 1

	# We tag our filesystem with a specific filesystem UUID
	ROOT_UUID=$(echo $rootfs_string | sed s/UUID=//)
	# busybox blkid can't tell LUKS volumes
	ROOT_DEV=$(blkid | grep $ROOT_UUID | cut -d':' -f 1)
	if [ -z "$ROOT_DEV" ]; then
		echo "No rootfs with rootfs=UUID check you /proc/cmdline:"
		cat /proc/cmdline
		exit 1
	fi

	echo "Found rootfs at $ROOT_DEV"
}

setup_env
find_root_device
try_encrypt || try_mount
update_parsec_config

# Move the mount points of some filesystems over to
# the corresponding directories under the real root filesystem.
for dir in `cat /proc/mounts | grep -v rootfs | awk '{ print $2 }'` ; do
	mkdir -p "$ROOTFS_MOUNT"/${dir##*/}
	mount -nv --move "$dir" "$ROOTFS_MOUNT"/${dir##*/}
done

cd "$ROOTFS_MOUNT"
switch_root="switch_root"

exec "$switch_root" "$ROOTFS_MOUNT" "$INIT" || {
	"$ROOTFS_MOUNT/bin/echo.coreutils" "Couldn't switch to the real rootfs"
	# Cause kernel panic.
	exit 2
}

exit 0
